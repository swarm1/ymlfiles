user  nginx;
worker_processes  auto;
load_module modules/ngx_stream_js_module.so;
pid        /var/run/nginx.pid;

events {
    worker_connections  1024;
}

http {
  include       /etc/nginx/mime.types;
  default_type  application/octet-stream;

  log_format  doh   '$remote_addr - $remote_user [$time_local] "$request" '
                    '[ $msec, $request_time, $upstream_response_time $pipe ] '
                    '$status $body_bytes_sent "$http_x_forwarded_for" '
                    '$upstream_http_x_dns_question $upstream_http_x_dns_type '
                    '$upstream_http_x_dns_result '
                    '$upstream_http_x_dns_ttl $upstream_http_x_dns_answers '
                    '$upstream_cache_status';

  access_log  /var/log/nginx/doh-access.log doh;
  error_log   /var/log/nginx/doh-error.log notice;

  upstream dohloop {
    zone dohloop 64k;
    server 127.0.0.1:8053;
    keepalive_timeout 60s;
    keepalive_requests 100;
    keepalive 10;
  }

  proxy_cache_path /var/cache/nginx/doh_cache levels=1:2 keys_zone=doh_cache:10m;

  server {
    listen 443 ssl http2;
    ssl_certificate /etc/nginx/ssl/certificates/doh.$DOMAINNAME.crt;
    ssl_certificate_key /etc/nginx/ssl/certificates/doh.$DOMAINNAME.key;
    ssl_dhparam /etc/nginx/ssl/acme/dhparam.pem;
    ssl_session_cache shared:ssl_cache:10m;
    ssl_session_timeout 10m;

    proxy_cache_methods GET POST;

    location / {
      return 404 "404 Not Found\n";
    }

    location /dns-query {
      proxy_http_version 1.1;
      proxy_set_header Connection "";
      proxy_cache doh_cache;
      proxy_cache_key $scheme$proxy_host$uri$is_args$args$request_body;
      proxy_pass http://dohloop;
    }
  }
}

stream {
  log_format  dns   '$remote_addr [$time_local] $protocol "$dns_qname"';
  access_log /var/log/nginx/dns-access.log dns;
  error_log  /var/log/nginx/dns-error.log notice;

  js_include /etc/nginx/njs.d/nginx_stream.js;

  js_set $dns_qname dns_get_qname;

  upstream dot {
    zone dot 64k;
    server 1.1.1.1:853;
  }

  server {
    listen 853 ssl;
    ssl_certificate /etc/nginx/ssl/certificates/dot.$DOMAINNAME.crt;
    ssl_certificate_key /etc/nginx/ssl/certificates/dot.$DOMAINNAME.key;
    ssl_dhparam /etc/nginx/ssl/acme/dhparam.pem;

    js_preread dns_preread_dns_request;

    proxy_ssl on;
    proxy_pass dot;
  }

  server {
    listen 127.0.0.1:8053;
    js_filter dns_filter_doh_request;
    proxy_ssl on;
    proxy_pass dot;
  }
}
