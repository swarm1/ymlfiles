# Enable Google TCP BBR
# Creates enable_google_tcp_bbr disable_google_tcp_bbr

mkdir -p ~/scripts
cd ~/scripts
touch enable_google_tcp_bbr
chmod +x enable_google_tcp_bbr

echo '
# Enable Google TCP BBR
# Tested on Ubuntu 20.04

major_version=$(uname -r | awk -F"." '\''{print $1}'\'')
minor_version=$(uname -r | awk -F"." '\''{print $2}'\'')

if [ "$major_version" -lt "4" ]
then
   echo "Kernel version is: $major_version.$minor_version which is too low please upgrade kernel to version 4.9.0 or greater"
   echo -e "\033[0;31mFailed\033[m" && exit 1;
else
   if [ "$major_version" -eq "4" ] && [ $minor_version -lt "9" ]
   then
      echo "Kernel version is: $major_version.$minor_version which is too low please upgrade kernel to version 4.9.0 or greater"
      echo -e "\033[0;31mFailed\033[m" && exit 1;
   else
      echo "Kernel version is: $major_version.$minor_version which is ok"
      fi
   fi

yes | sudo cp /etc/sysctl.conf /etc/sysctl.conf.bak

if sudo grep -Fq "net.core.default_qdisc=fq" /etc/sysctl.conf
then
:
else
   echo "net.core.default_qdisc=fq" | sudo tee -a /etc/sysctl.conf
fi

if sudo grep -Fq "net.ipv4.tcp_congestion_control=bbr" /etc/sysctl.conf
then
:
else
   echo "net.ipv4.tcp_congestion_control=bbr" | sudo tee -a /etc/sysctl.conf
fi

# show differences
# sudo diff /etc/sysctl.conf /etc/sysctl.conf.bak

sudo sysctl -qp

if sudo sysctl net.ipv4.tcp_congestion_control | grep -q "net.ipv4.tcp_congestion_control = bbr" ; then echo -e "\033[0;32mSuccess\033[m" && exit 0; else echo -e "\033[0;31mFailed\033[m" && exit 1; fi

' >~/scripts/enable_google_tcp_bbr

./enable_google_tcp_bbr

touch disable_google_tcp_bbr
chmod +x disable_google_tcp_bbr

cat >~/scripts/disable_google_tcp_bbr <<EOF
# remove net.core.default_qdisc=fq from /etc/sysctl.conf
# remove net.ipv4.tcp_congestion_control=bbr from /etc/sysctl.conf

if sudo grep -Fq "net.core.default_qdisc=fq" /etc/sysctl.conf
then
   sudo sed -i "s/^net\.core\.default_qdisc\=fq//" /etc/sysctl.conf
   sudo sed -Ezi '$ s/\n+$//' /etc/sysctl.conf
   echo -e "" | sudo tee -a /etc/sysctl.conf
   echo "net.core.default_qdisc=fq removed from /etc/sysctl.conf"
else
   echo "net.core.default_qdisc=fq not found in /etc/sysctl.conf"
   fi
if sudo grep -Fq "net.ipv4.tcp_congestion_control=bbr" /etc/sysctl.conf
then
   sudo sed -i "s/^net\.ipv4\.tcp_congestion_control\=bbr//" /etc/sysctl.conf
   sudo sed -Ezi '$ s/\n+$//' /etc/sysctl.conf
   echo -e "" | sudo tee -a /etc/sysctl.conf
   echo "net.ipv4.tcp_congestion_control=bbr removed from /etc/sysctl.conf"
else
   echo "net.ipv4.tcp_congestion_control=bbr not found in /etc/sysctl.conf"
   fi

sudo sysctl -qp

if ! sudo grep -Fq "net.core.default_qdisc=fq" /etc/sysctl.conf && ! sudo grep -Fq "net.ipv4.tcp_congestion_control=bbr" /etc/sysctl.conf ; then echo -e "\033[0;32mSuccess\033[m" ; exit 0; else echo -e "\033[0;31mFailed\033[m" ; exit 1; fi
EOF

ls
